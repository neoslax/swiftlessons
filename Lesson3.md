# Lesson 3
```
import UIKit

let names = ["Sasha","Vasya", "DashaDasha", "Dasha"]

let unsorted = [5,1, 5, 5, 1,424,1,-12]

let sortedByAsc = unsorted.sorted(by: {$0 < $1})

let sortedByDesc = unsorted.sorted(by: {$0 > $1})
print(sortedByAsc)
print(sortedByDesc)

print(names.sorted(by: {$0.count < $1.count}))

func keysToMap(keys: String) -> [String: String] {
    return [keys: "value = \(keys)"]
}

keysToMap(keys: "Key")

```
