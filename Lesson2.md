# Массивы

```
import UIKit

let days = [31, 28, 31,30,31,30,31,31,30,31,30,31]
let month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

for day in days {
    print(day)
}

for (index, day) in days.enumerated()	 {
    print("\(month[index]): \(day)")
}
print("Another loop")
for i in 0..<month.count    {
    print("\(month[i]): \(days[i])")
}

for i in days.reversed() {
    print(i)
}

// for Mar, 25
var dayCounter = 0
var counter = 0
for i in month {
    if (i == "Mar") {
        dayCounter += 25
        break
    } else {
        dayCounter += days[counter]
        counter = counter + 1
    }
}
print("Days to Mar, 25 = \(dayCounter)")

```
