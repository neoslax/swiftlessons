# Les4


```
import UIKit
enum Action {
    case Left
    case Right
}

enum Form {
    case FullName
    case Age
    case Experience
}

enum Rating {
    case A
    case B
    case C
}

func markSystem(name: String, rate: Rating) {
   switch (rate) {
        case .A:
            print(name + " A")
        case .B:
            print(name + " B")
        case .C:
            print(name + " C")
    }
}

markSystem(name: "Ivan", rate: Rating.A)

```
